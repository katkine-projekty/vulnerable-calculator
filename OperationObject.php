<?php
class OperationObject {
    private $operation = '';
    private $operands = ['+', '-', '*', '/', '**', '%'];

    public function __construct() {
    }

    public function fromString($operationString) {
        $this->operation = $operationString;
        $valid = false;

        foreach($this->operands as $op) {
            if (strpos($this->operation, $op)) {
                $valid = true;
                break;
            }
        }

        if (!$valid) throw new Exception('Operation does not contain any operands please check the input.');

        return $this;
    }

    public function calculate() {
        $result = eval('return ' . $this->operation . ';');
        if (error_get_last()) throw new Exception(error_get_last()['message']);
        return $result;
    }

    public function toString() {
        return $this->operation;
    }
}