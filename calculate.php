<?php
include_once('OperationObject.php');

if(isset($_POST['submit']))
{
    session_start();
    try {
        $operation = (new OperationObject())->fromString($_POST["operation"]);
        $_SESSION['input'] = $operation->toString();
        $_SESSION['result'] = $operation->calculate();
    } catch (Exception $ex) {
        $_SESSION['result'] = null;
        $_SESSION['error'] = $ex->getMessage();
    }

    header('Location: index.php');
} 