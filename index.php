<!DOCTYPE html>
<html>

<head>
  <title>Calculate function</title>
  <meta charset="utf-8" />
  <link rel="stylesheet" href="bootstrap.min.css" type="text/css" media="all" />
  <link rel="stylesheet" href="custom.css" type="text/css" media="all" />
  <link rel="stylesheet" href="animate.css" type="text/css" media="all" />
</head>

<body>
  <div class="container">
    <header>
      <div class="page-header">
        <h1>Vulnerable calculator</h1>
      </div>
    </header>
    <div class="row">
      <div class="col-md-4 col-md-offset-2">
        <div id="calculatorUi">
          <div class="numeric">
          </div>
          <hr />

          <div class="operands">
          </div>
        </div>
      </div>
      <div class="col-md-4">

        <p>
          Please enter simple mathematic operation but keep in mind that this system is really easy to attack.
        </p>

        <?php session_start(); ?>

        <?php if(isset($_SESSION['error'])): ?>
        <div class="panel panel-danger">
          <div class="panel-heading">
            <h3 class="panel-title">Error</h3>
          </div>
          <div class="panel-body text-center">
            <?php print_r($_SESSION['error']); ?>
          </div>
        </div>
        <?php endif ?>

        <form method="post" action="calculate.php" id="calc_form">
          <div class="form-group">
            <label for="function">Mathematic operation :</label>
            <input id="operation-input" class="form-control" type="text" name="operation" autocomplete="off" placeholder="2*(5+3)" required/>
          </div>

          <button class="btn btn-primary" type="submit" name="submit">Calculate</button>
        </form>
        </br>

        <?php if(isset($_SESSION['result'])): ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Calculation result</h3>
          </div>
          <div class="panel-body text-center">
            <p>
              <b>Input : </b>
            <?php
              print_r($_SESSION['input']); 
            ?>
            </p>
            <p>
            <b>Result :</b>
            <?php
							print_r($_SESSION['result']); 
						?>
            </p>
          </div>
        </div>
        <?php endif; ?>

        <?php session_unset(); ?>
      </div>
    </div>
  </div>
  <script src="jquery-1.12.3.min.js"></script>
  <script>
    $(document).ready(function() {
      var operations = {
        '+' : '+',
        '-' : '-',
        '*' : '*',
        ':' : '/',
        'x<sup>y</sup>' : '**'
      }
      
      for (var i = 9; i >= 0; i--) {
        addSymbol($('#calculatorUi .numeric'), i);
      }

      for(var key in operations) {
        addSymbol($('#calculatorUi .operands'), key, operations[key]);
      }

      function addSymbol(elem, sym, sym_alt = null) {
        $(elem).append('<a class="btn btn-default" href="javascript:;" data-value="' + (sym_alt || sym) + '">' + sym + '</a>')
      }

      $('#calculatorUi a').click(function() {
        $('#operation-input').val($('#operation-input').val() + $(this).data('value'));
      });

      $('#operation-input').keydown(function(e) {
        var elem = $('#calculatorUi').find("[data-value='" + e.key + "']");
        elem.trigger('mouseenter');
      });
    });
  </script>
</body>

</html>